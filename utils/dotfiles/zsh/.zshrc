# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="aussiegeek"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"
HIST_STAMPS="dd.mm.yyyy"
HISTFILE=$HOME/.zsh_history
HISTSIZE=1000000000
SAVEHIST=1000000000

setopt BANG_HIST                 # Treat the '!' character specially during expansion.
setopt EXTENDED_HISTORY          # Write the history file in the ":start:elapsed;command" format.
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY             # Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS          # Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS      # Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS         # Do not display a line previously found.
setopt HIST_IGNORE_SPACE         # Don't record an entry starting with a space.
setopt HIST_SAVE_NO_DUPS         # Don't write duplicate entries in the history file.
setopt HIST_REDUCE_BLANKS        # Remove superfluous blanks before recording entry.
setopt HIST_VERIFY               # Don't execute immediately upon history expansion.
#setopt HIST_BEEP                 # Beep when accessing nonexistent history.

# GENERAL
# AUTOCOMPLETION
# initialize autocompletion
autoload -U compinit
compinit
# autocompletion using arrow keys (based on history)
bindkey '\e[A' history-search-backward
bindkey '\e[B' history-search-forward

# (bonus: Disable sound errors in Zsh)
# never beep
setopt NO_BEEP

# # new ones 
# 
# setopt INC_APPEND_HISTORY
# export HISTTIMEFORMAT="[%F %T] "
# setopt EXTENDED_HISTORY
# setopt HIST_FIND_NO_DUPS
# setopt HIST_IGNORE_ALL_DUPS

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
#  debian
  asdf
  aws
  rsync
#  torrent
  docker
  docker-machine
  docker-compose
  gpg-agent
  rsync
  tmux
#  vim-interaction
#  vi-mode
#  virtualenv
  minikube
  git
#  github
  sudo 
#  python
#  pip
  history
  git-extras
  httpie
#  pyenv
  kubectl
#  scala
  vscode
#  z
  ubuntu
  systemd
  git-extra-commands # testing this out https://github.com/unixorn/git-extra-commands 
#  fly
# kube-ps1
# kubetail
#  zsh-autosuggestions
# history-sync
# autoswitch_virtualenv $plugins
  fzf-zsh-plugin
)

# auto update settings 
export DISABLE_AUTO_UPDATE="true"

source $ZSH/oh-my-zsh.sh
# needed by plugins to be set 
export ZSH_CACHE_DIR=$ZSH/cache

# decrease omz update verbosity
zstyle ':omz:update' mode disabled    
zstyle ':omz:update' verbosity minimal

# Example aliases
# alias zshconfig="mate $HOME/.zshrc"
# alias ohmyzsh="mate $HOME/.oh-my-zsh"

# source profiles main (change to os specific)
source $HOME/.zsh_profile_alias # contains all aliases 
source $HOME/.zsh_profile_linux # contains os specific conf

# source work files 
#source $HOME/utils/private/zsh/.zsh_profile_work # contains work config

# starship conf
export STARSHIP_CACHE=$HOME/starship.cache
export STARSHIP_CONFIG=$HOME/starship.toml
eval "$(starship init zsh)"
# welcome message
#macchina
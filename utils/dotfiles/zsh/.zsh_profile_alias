
# system update aliases
alias update-my-system='source ~/utils/scripts/update_my_system.sh'
alias delete-lock-apt='sudo rm /var/lib/apt/lists/lock && sudo rm /var/cache/apt/archives/lock && sudo rm /var/lib/dpkg/lock'

# gpg agent related aliases
alias reload-yubikey='gpg-connect-agent "scd serialno" "learn --force" /bye'
alias reload-gpg-agent='gpg-connect-agent reloadagent /bye'
alias reload-pcsd='sudo service pcscd restart'
yubikey-reset() {
    reload-pcsd
    reload-gpg-agent
    reload-yubikey
}

# docker aliases 
alias docker-stop-containers='docker kill $(docker ps -q)'
alias docker-remove-containers='docker rm $(docker ps -a -q)'
alias docker-delete-images='docker rmi $(docker images -q)'
alias docker-delete-volumes=' docker volume ls -qf dangling=true | xargs -r docker volume rm'
alias docker-delete-all='docker ps -q | xargs docker stop ; docker system prune -a'
docker-delete-app() {
 docker rm (docker ps -a |grep "$1" |awk '{print $1}')
}

# vpn windscribe , tailscale 
wind-up() {
    windscribe connect '{print $1}'
}

wind-firewall() {
    windscribe firewall '{print $1}'
}

wind-wireguard() {
    echo " this will make tailscale unusable"
    sudo wg-quick "$1" wg."$2"
}

tail-vpn-restart() {
    sudo tailscale down
    sudo tailscale up
    sudo tailscale status
    nslookup "$1"
}

tail-fix-dns() {
    sudo systemctl restart systemd-resolved
    sudo systemctl restart NetworkManager
    sudo systemctl restart tailscaled
}

dns-reset1() {
    sudo systemctl restart systemd-resolved
    sudo systemctl restart resolvconf
}

dns-reset-all() {
    sudo systemctl restart systemd-resolved
    sudo systemctl restart NetworkManager
    sudo systemctl restart tailscaled
    sudo systemctl restart resolvconf
}

# random aliases 

search-env() {
    printenv | grep -i "$1"
}
count-lines() {
    sed '/^$/d'| awk '{print NR}'| sort -nr| sed -n '1p'
}
alias apps-killall='source ~/projects/personal/scripts/bootstrap/various_scripts/kill-relevant-apps.sh'
alias ntp-force-update='sudo date -s "$(wget -qSO- --max-redirect=0 google.com 2>&1 | grep Date: | cut -d' ' -f5-8)Z"'
alias myrsync='sudo rsync -xah --info=progress2 --no-i-r'

# uptime 
uptime-mine() {
    awk '{print int($1/3600)":"int(($1%3600)/60)":"int($1%60)}' /proc/uptime
}


# dev aliases 

# typo aliases  
alias suod='sudo'
alias rsynx='rsync'

# tmuxinator aliases 
alias mux="tmuxinator"

# git log aliases 
alias git-log="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
alias git-author='git log --pretty=format:"%h%x09%an%x09%ad%x09%s"'

# shell aliases to search for large files/folders
alias ducks='sudo du -ckhs * | sort -rn | head'

# alias unset aws and kubeconfig 
dev-unset-vars(){
    unset AWS_PROFILE
    unset KUBECONFIG
}

# test entries 

alias myls='ls --color=tty'
alias mygrep='grep  --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}'
alias -s {yml,yaml}=vim

my-net-ip(){
  colorprintf purple "IpV4"
  ip -4 addr | grep -oP '(?<=inet\s)\d+(\.\d+){3}'
  colorprintf purple "IpV6"
  ip -6 addr | grep -oP '(?<=inet6\s)[\da-f:]+'
  colorprintf purple "My Public Ip"
  #dig +short txt ch whoami.cloudflare @1.0.0.1
  dig @ns1-1.akamaitech.net ANY whoami.akamai.net +short
  curl -s ifconfig.me/ip | cut -f1 -d"%"
  wget -qO - icanhazip.com
  colorprintf purple "My tailscale ip"
  tailscale ip -4
}

weather() {
    #curl v2.wttr.in               # print weather for current location (https://github.com/chubin/wttr.in)
    curl wttr.in
    #curl v2.wttr.in 
    curl -s 'wttr.in/{chq,London,Athens,Santa+Clara,~Cupertino}?format=3'
    #curl -s 'wttr.in/{chq,London,Athens,Santa+Clara,~Cupertino}?format="%l:+%c,+%t,f:+%f+%C+%h+%w"'
    #curl -s 'wttr.in/chq?format="%l:+%c,+%t,f:+%f+%C+%h+%w"'
    #curl wttr.in/chq
    curl wttr.in/chq
    #curl v3.wttr.in/Crete.sxl
    #curl v3.wttr.in/Athens.sxl
    #curl v3.wttr.in/London.sxl
    #curl v3.wttr.in/California.sxl
}

# replacement commands 
#alias cat="batcat"
alias mycat='batcat --style=header,grid'

alias ll="eza -l"
alias lll="eza -la"
alias l="eza"
alias tree="eza -TRabghlUm"
alias ls="eza -abghlUm"

alias df="duf"
alias man="tldr"
alias du="ncdu"

alias z="zoxide"
#alias cd="zoxide"
#alias zz="z -"

alias clipboard='xclip -selection clipboard -i'

#mycat () {
#  rich "$1" -n -g
#}

# aws 
aws-s3-copy() {
aws s3 cp $1 .
}
aws-ec2-show-public-ip() {
aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId,Tags[?Key==`Name`].Value|[0],State.Name,PrivateIpAddress,PublicIpAddress]' --output text | column -t | grep -i $1
}

#Argocd Functions
function argocdpw() {
  kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
  echo ""
}

function argocdport() {
  kubectl port-forward service/argocd-server -n argocd 8080:443
}

# switch between aws config files
set-awsconfig-okta() {
    echo "" > ~/.aws/config
    cp ~/.aws/aws_config_okta ~/.aws/config
}

set-awsconfig-sso() {
    echo "" > ~/.aws/config
    cp ~/.aws/aws_config_sso ~/.aws/config
}

# teleport aliases
alias tout='tsh logout'
alias tkls='tsh kube ls'


# update diffent system components

function update-my-deleteAptLock (){
    # check for apt
    if [ -f /usr/bin/apt ]; then
        sudo rm /var/lib/apt/lists/lock
        sudo rm /var/cache/apt/archives/lock
        sudo rm /var/lib/dpkg/lock
    fi
}

function update-my-apt () {
    # check for apt
    if [ -f /usr/bin/apt ]; then
        colorprintf yellow "Update Apt Packages"
        sudo apt -qqy update
        colorprintf yellow "Upgrade Apt Packages"
        sudo apt -qqy full-upgrade
        colorprintf yellow "Autoremove Apt Packages"
        sudo apt -qqy autoremove 
        colorprintf yellow "Autoclean Apt Packages"
        sudo apt -qqy autoclean
        colorprintf green "Updated Apt Packages"
    fi
}

function update-my-apt-nala () {
    # check for nala 
    if [ -f /usr/bin/nala ]; then
        colorprintf yellow "Update Apt Packages using Nala"
        # update system using nala
        sudo nala upgrade -y
        sudo nala autoremove -y
        sudo nala clean
    fi
}

function update-my-snap () {
    # check for snap
    if [ -f /usr/bin/snap ]; then
        colorprintf yellow "Update Snap Packages"
        sudo snap refresh
        colorprintf green "Updated Snap Packages"
    fi
}

function update-my-flatpak () {
    # check for flatpak
    if [ -f /usr/bin/flatpak ]; then
        colorprintf yellow "Update Flatpak Packages"
        flatpak update -y
        colorprintf green "Updated Flatpak Packages"
    fi
}

function update-my-gnome-Extensions () {
    # check for gnome-shell-extension-installer
    if [ -f /usr/bin/gnome-shell-extension-installer ]; then
        colorprintf yellow "Update Gnome Extensions"
        gnome-shell-extension-installer --yes --update --restart-shell
        colorprintf green "Updated Gnome Extensions"
    fi
    # check for gext
    if [ -f /home/kfp/.local/bin/gext ]; then
        colorprintf yellow "Update Gnome Extensions"
        gext update
        colorprintf green "Updated Gnome Extensions"
    fi
}

function update-my-pacman () {
    # check for pacman 
    if [ -f /usr/bin/pacman ]; then
        colorprintf yellow "Create list of installed packages before update"
        # generate list of installed packages 
        today=`date +%d-%m-%Y.%H:%M:%S`
        sudo pacman -Qqe > $HOME/Documents/Pacman_list_of_installed_software_pre_update_${today}.txt

        colorprintf yellow "Refresh Pacman Repos"
        sudo pacman --sync --refresh

        colorprintf yellow "Refresh Keyrings"
        sudo pacman --sync --needed archlinux-keyring

        colorprintf yellow "Update Pacman Packages"
        colorprintf yellow "Remove Orphan Packages"
        # Removing All Orphans If there are many orphans, you can remove all of them with the following command:
        pacman -Qtdq | sudo pacman -Rns -

        colorprintf yellow "Create list of installed packages after update"
        # generate list of installed packages 
        today=`date +%d-%m-%Y.%H:%M:%S`
        sudo pacman -Qqe > $HOME/Documents/Pacman_list_of_installed_software_after_update_${today}.txt
    fi

    # check for paru
    if [ -f /usr/bin/paru ]; then
        colorprintf yellow "Update Packaged using Pacman + Paru "
        sudo pacman -Sy --noconfirm && paru -Su --noconfirm
    fi

    # check for eos-update 
    if [ -f /usr/bin/eos-update ]; then
        colorprintf yellow "Update EndeavourOS"
        sudo pacman --sync --needed endeavouros-keyring
        eos-update
    fi
}

function update-my-debian-apt () {
    update-my-deleteAptLock
    update-my-apt
}
function update-my-debian-nala () {
    update-my-deleteAptLock
    update-my-apt-nala
}


function update-my-fedora () {
    # check for dnf
    if [ -f /usr/bin/dnf ]; then
        colorprintf yellow "Update Fedora"
        sudo dnf update -y && sudo dnf autoremove -y
    fi
}

function update-my-opensuse () {
    # check for zypper
    if [ -f /usr/bin/zypper ]; then
        colorprintf yellow "Update Opensuse"
        sudo zypper --non-interactive --quiet ref && sudo zypper --non-interactive --quiet dup
        sudo zypper --non-interactive  --quiet patch
    fi
}

function update-my-omz () {
    # omz
    # check if a folder exists ~/.oh-my-zsh
    if [ -d "$HOME/.oh-my-zsh" ]; then
        colorprintf yellow "Update OMZ"
        omz update
    fi

}

function update-my-krew () {
    # check for krew
    # Try to list kubectl plugins using Krew
    kubectl krew list > /dev/null 2>&1
    # Check if the previous command was successful
    if [ $? -eq 0 ]; then
        colorprintf yellow "Update krew plugins"
        kubectl krew upgrade > /dev/null 2>&1
        kubectl krew list
    fi
}

function update-my-asdf () {
    # check for asdf
    if [ -f $HOME/.asdf/bin/asdf ]; then
        colorprintf yellow "Update asdf"
        asdf update
    fi
}

function update-my-brew () {
    # check for brew
    brew --version > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        colorprintf yellow "Update brew"
        # update system using brew
        brew update 
        brew upgrade
        brew autoremove
        brew cleanup --prune=all
    fi
}

function update-my-fzf () {
    # check for fzf
    if [ -f $HOME/.fzf/bin/fzf ]; then
        colorprintf yellow "Update fzf"
        $HOME/.fzf/install --all --completion --key-bindings --no-update-rc
    fi
}

function update-my-devbox () {
    # check for devbox
    devbox version > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        colorprintf yellow "Update devbox"
        devbox version update
        devbox update
        devbox global update
    fi
}

function update-my-pipx () {
    # check for pipx
    if [ -f /bin/pipx ]; then
        colorprintf yellow "Update pipx"
        pipx upgrade-all
    fi
}